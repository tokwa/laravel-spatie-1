@extends('layouts.app')

@section('content')
@if(count($posts))
<div class="container">
    <ul class="list-group">
        <li class="list-group-item text-center">
            <h3>POSTS</h3>
        </li>
        @foreach($posts as $post)
        <li class="list-group-item">
            <div class="row">
                <div class="col-6">
                    {{$post->title}}
                </div>
                <div class="col-6 text-right">
                    <form method="POST" id="deleteForm" action="{{route('posts.destroy', $post->id)}}">
                        <a class="btn btn-primary" href="{{route('posts.show', $post->id)}}">Show</a>
                        <a class="btn btn-success" href="{{route('posts.edit', $post->id)}}">Edit</a>
                        @method('DELETE')
                        @csrf
                        <button id="deleteBtn" type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>
@else
<p>
    There is no post at the moment.
</p>
@endif
@endsection