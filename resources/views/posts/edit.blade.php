@extends('layouts.app')

@section('content')
<div class="container">
    <form action="method" id="editForm" action="">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Post Title</label>
        <input type="text" class="form-control" id="title" name="title" aria-describedby="title-Help" value="{{$post->title}}">
            <div class="invalid-feedback" id="err-title" role="alert"></div>
            <small id="titleHelp" class="form-text text-muted">Title of the post.</small>
        </div>
        <div class="form-group">
            <label for="body">Post Body</label>
        <textarea class="form-control" id="body" name="body" rows="3">{{$post->body}}</textarea>
            <div class="invalid-feedback" id="err-body" role="alert"></div>
            <small id="titleHelp" class="form-text text-muted">Post content.</small>
        </div>
        <div class="form-group">
            <a class="btn btn-danger" href="{{URL::previous()}}">Cancel</a>
            <button type="submit" id="submitBtn" class="btn btn-primary">
                {{ __('Submit') }}
            </button>
        </div>
    </form>
</div>

<script>
$(document).ready( () => {
    document.getElementById('editForm').addEventListener('submit', function (e) {

        e.preventDefault()

        $('#submitBtn').html('<i class="fas fa-spinner fa-spin"></i> Loading ..')
        $('#submitBtn').prop('disabled', true)

        const x = new FormData()
        x.append('title', document.getElementById('title').value)
        x.append('body', document.getElementById('body').value)
        x.append('_method', 'PUT')

        axios.post("{{ route('posts.update', $post->id) }}", x)
            .then((res) => {
                alert(res.data.sms)
                window.location.href = "{{ route('posts.index') }}"
            })
            .catch((error) => {

                console.log(error.response)

                $('#submitBtn').html("<i class='fas fa-save'></i> Submit")
                $('#submitBtn').prop('disabled', false)
                // $('#cancelUpdate').prop('disabled', false)
                
                const errBoxes = document.getElementsByClassName('form-control')
                const errSms = document.getElementsByClassName('invalid-feedback')
                Array.from(errBoxes).forEach(el => el.classList.remove('is-invalid'))
                Array.from(errSms).forEach(el => el.innerHTML = "")
                Array.from(errSms).forEach(el => el.style.display = "none")

                if (error.response.data.errors.title) {
                    let box = document.querySelector('#title')
                    let sms = document.getElementById('err-title')
                    sms.innerHTML = error.response.data.errors.title[0]
                    sms.style.display = "block"
                    box.classList.add('is-invalid')
                }

                if (error.response.data.errors.body) {
                    let box = document.querySelector('#body')
                    let sms = document.getElementById('err-body')
                    sms.innerHTML = error.response.data.errors.body[0]
                    sms.style.display = "block"
                    box.classList.add('is-invalid')
                }

            })
        })
    });
</script>
@endsection