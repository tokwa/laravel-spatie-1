@extends('layouts.app')

@section('content')
<div class="container d-flex justify-content-center">
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">{{$post->title}}</h5>
            <p class="card-text">{{$post->body}}</p>
        <a class="btn btn-primary" href="{{ URL::previous() }}">Back</a>
        </div>
    </div>
</div>
@endsection