<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index()
    {
       $posts = Post::orderBy('created_at', 'desc')->paginate(10);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
        ]);

        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->id();

         if ($post->save()) {

            return response()->json([
                        'status' => 200,
                        'sms' => 'Post Created!',
                    ]);
        }   else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong!'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $post)
    {
        if (! request()->ajax() && 
        !request()->isSecure()) {
            return response()->json(['error' => 'Failed to update.'], 500);
        }

        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
        ]);

            $x = Post::findOrFail($post);
            $x->title = $request->title;
            $x->body = $request->body;

        if ($x->save()) {

            return response()->json([
                'status' => 200,
                'sms' => 'Successfully Edited!!',
            ]);
        } 
        else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong!!'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete($post);
            
        return redirect()->route('posts.index')->with('success', 'Post Deleted!');
    }
}
