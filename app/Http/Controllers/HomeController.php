<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        // $user = auth()->user()->getAllPermissions();
        // $user = User::role('writer')->get();

        // return auth()->user()->removeRole('writer');

        // dd(json_encode($user, JSON_PRETTY_PRINT));

        


        return view('home');

        // Role::create(['name' => 'writer']);
        // Permission::create(['name' => 'edit post']);
        // $permission = Permission::create(['name' => 'write post']);
        // $permission = Permission::findById(1);
        // $role = Role::findById(1);
        // $role->givePermissionTo($permission);
        // auth()->user()->givePermissionTo('edit post');
        // auth()->user()->assignRole('writer');
        // return auth()->user()->permissions;

        // $user = auth()->user();

        // $permissions = $user->getDirectPermissions();

        // return dd(json_encode($permissions, JSON_PRETTY_PRINT));

        // Role::create(['name'=>'writer']);
        // Permission::create(['name' => 'write post']);
        // $permission = Permission::create(['name' => 'edit post']);
        // $role = Role::findById(1);
        // $permission->assignRole($role);
        // $permission = Permission::findById(1);
        // $role->givePermissionTo($permission);
        // $role->syncPermissions($permission);
        // $permission->removeRole($role);
    }
}
